package treinamento.altamax;

/**
 * Created by paulo on 14/01/2018.
 */

public class Device {
    private String name;
    private String ip;
    private String mac;
    private int state;
    private double power;
    private double current;
    private double voltage;

    public Device(String name, String IP,int state,String mac){
        name=name;
        ip=ip;
        state=state;
        mac=mac;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public double getCurrent() {
        return current;
    }

    public void setCurrent(double current) {
        this.current = current;
    }

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

}
