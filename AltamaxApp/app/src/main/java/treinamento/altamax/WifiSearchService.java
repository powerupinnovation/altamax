package treinamento.altamax;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.preference.PreferenceActivity;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.AsyncSocket;
import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.Util;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.callback.ConnectCallback;
import com.koushikdutta.async.callback.DataCallback;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.AsyncHttpGet;
import com.koushikdutta.async.http.AsyncHttpPost;
import com.koushikdutta.async.http.AsyncHttpResponse;
import com.koushikdutta.async.http.callback.HttpConnectCallback;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;





public class WifiSearchService extends IntentService {

    private static final String TAG = WifiSearchService.class.getName();

    private Messenger messageHandler;

    private String requestresult;

    private String devicemeasstatus;

    ResultReceiver receiver;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public WifiSearchService() {
        super(TAG);
        requestresult = "none";
    }



    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

       // Uri uri = intent.getData();

        int requestType = intent.getIntExtra(Constants.INTENT_KEY_REQUEST_TYPE,0);

        receiver = intent.getParcelableExtra(Constants.INTENT_KEY_RECEIVER);

        String currentip = intent.getStringExtra(Constants.INTENT_KEY_MOBILE_IP);

        Bundle resultBundle = new Bundle();

        //resultBundle = new Bundle();

        switch(requestType)
        {
            case Constants.MSG_MOBILE_IP:
                resultBundle.putString(Constants.INTENT_KEY_REQUEST_RESULT,getIPAddress(true));
                receiver.send(Constants.MSG_MOBILE_IP, resultBundle);
                devicemeasstatus="none";
            break;


            case Constants.MSG_UPDATE_DEVICES_LIST:
                 try {
                      NetworkInterface iFace = NetworkInterface.getByInetAddress(InetAddress.getByName(currentip));
                      for (int i = 0; i <= 255; i++) {
                        String addr = currentip;
                        addr = addr.substring(0, addr.lastIndexOf('.') + 1) + i;
                        InetAddress pingAddr = InetAddress.getByName(addr);
                        if (pingAddr.isReachable(iFace, 200, 100)) {
                            getservicebyip(pingAddr.getHostAddress());
                            Log.d(TAG, pingAddr.getHostAddress());
                        }
                    }
                    Log.d(TAG,"Completed");
                    resultBundle.putInt(Constants.INTENT_KEY_REQUEST_PROGRESS,100);
                    receiver.send(Constants.MSG_UPDATE_DEVICES_LIST_COMPLETE, resultBundle);

                } catch (UnknownHostException ex) {
                } catch (IOException ex) {
                }
            break;

            case Constants.MSG_UPDATE_DEVICE_STATUS:
                String devicestatus  = intent.getStringExtra(Constants.INTENT_KEY_DEVICE_STATUS);
                String ip  = intent.getStringExtra(Constants.INTENT_KEY_DEVICE_IP);
                setdevicestatus(ip,devicestatus);
            break;

            case Constants.MSG_GET_DEVICE_STATUS:
                String deviceiptmp  = intent.getStringExtra(Constants.INTENT_KEY_DEVICE_IP);
                Log.d(TAG,deviceiptmp);
                getdevicestatus(deviceiptmp);
            break;

            case Constants.MSG_UPDATE_KWH:
            break;

        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }




    //TODO get current device ip
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {

            Log.e(TAG,"Cannot find ip");
        } // for now eat exceptions
        return "";
    }


    //TODO find service by ip
    public String getservicebyip(String ip) {
        final AsyncHttpGet get = new AsyncHttpGet("http://"+ip);
        AsyncHttpClient.getDefaultInstance().executeString(get, new AsyncHttpClient.StringCallback() {
            @Override
            public void onCompleted(Exception e, AsyncHttpResponse source, String result) {
                if (e != null) {
                    requestresult = "none";
                    e.printStackTrace();
                    return;
                }
                System.out.println("I got a string: " + result.toString());
                if (result.contains("esp8266")){
                    System.out.println("Device found");
                    requestresult = "found:"+get.getUri().getHost().toString();
                    if (requestresult.contains("found")){
                        Bundle bundle = new Bundle();
                        String[] iphost = requestresult.split(":");
                        System.out.println("I got a string: " + iphost[1]);
                        bundle.putString(Constants.INTENT_KEY_REQUEST_RESULT,iphost[1]);
                        bundle.putInt(Constants.INTENT_KEY_REQUEST_PROGRESS,0);
                        receiver.send(Constants.MSG_UPDATE_DEVICES_LIST, bundle);
                        requestresult="none";
                    }
                    System.out.println(requestresult);
                    return;

                }
                System.out.println("Device not found ");

            }
        });
        return "finished";
    }


    public void setdevicestatus(String ip, String status) {
        System.out.println("http://"+ip+"/"+status);
        AsyncHttpGet get = new AsyncHttpGet("http://"+ip+"/"+status);
        AsyncHttpClient.getDefaultInstance().executeString(get, new AsyncHttpClient.StringCallback() {
            @Override
            public void onCompleted(Exception e, AsyncHttpResponse source, String result) {
                if (e != null) {
                    e.printStackTrace();
                    return;
                }
                Log.d(TAG,"I got a string: " + result);
            }
        });
        return;
    }

    public void getdevicestatus(String ip) {
        AsyncHttpGet get = new AsyncHttpGet("http://"+ip+"/getstatus");
        AsyncHttpClient.getDefaultInstance().executeString(get, new AsyncHttpClient.StringCallback() {
            @Override
            public void onCompleted(Exception e, AsyncHttpResponse source, String result) {
                if (e != null) {
                    e.printStackTrace();
                    return;
                }
                Log.d(TAG,result);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.INTENT_KEY_DEVICE_INFO, result);
                receiver.send(Constants.MSG_UPDATE_KWH, bundle);
            }
        });
        return;
    }



    //TODO Interface de comunicação
    public void sendMessage(int state) {
        Message message = Message.obtain();
        message.arg1 = state;
        try {
            messageHandler.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


}
