package treinamento.altamax;

/**
 * Created by paulo on 08/01/2018.
 */

public interface Constants {

    // Message types sent from the BluetoothChatService Handler
    public static final int MSG_UPDATE_DEVICE_STATUS = 1;
    public static final int MSG_UPDATE_KWH = 2;
    public static final int MSG_FIND_DEVICES_BY_IP=3;
    public static final int MSG_UPDATE_DEVICES_LIST=4;
    public static final int MSG_MOBILE_IP=5;
    public static final int MSG_GET_DEVICE_STATUS = 6;
    public static final int MSG_UPDATE_DEVICES_LIST_COMPLETE = 7;




    public static final String INTENT_KEY_REQUEST_TYPE = "INTENT_KEY_REQUEST_TYPE";
    public static final String INTENT_KEY_RECEIVER = "INTENT_KEY_RECEIVER";
    public static final String INTENT_KEY_DEVICE_STATUS = "INTENT_KEY_DEVICE_STATUS";
    public static final String INTENT_KEY_REQUEST_RESULT ="BUNDLE_KEY_REQUEST_RESULT";
    public  static final String INTENT_KEY_REQUEST_PROGRESS ="REQUEST_PROGRESS";
    public  static final String INTENT_KEY_DEVICE_IP ="INTENT_KEY_DEVICE_IP";
    public  static final String INTENT_KEY_DEVICE_INFO ="INTENT_KEY_DEVICE_INFO";
    public  static final String INTENT_KEY_MOBILE_IP ="INTENT_KEY_DEVICE_IP";

}
