package treinamento.altamax;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

//import static treinamento.altamax.R.id.lvdeviceslist;

public class MainActivity extends AppCompatActivity {

    private ResultReceiver mReceiver;

    private static final String TAG = "AppCompatActivity";

    private String mobileip;

    ListView listaDispositivos;

    List<String> slistaDispositivos;

    ArrayAdapter<String> madapter;

    ProgressBar mProgressBar;

    int devicestate;
    String deviceip;
    /** Flag indicating whether we have called bind on the service. */
    Intent intentwifiservice;

    public MainActivity(){
        mReceiver = new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                int state = resultCode;
                switch (state){
                    case Constants.MSG_MOBILE_IP:
                        mobileip = resultData.getString(Constants.INTENT_KEY_REQUEST_RESULT);
                        Toast.makeText(getApplicationContext(),"IP "+mobileip,Toast.LENGTH_SHORT).show();
                        Log.d(TAG,"MSG_MOBILE_IP");
                        Log.d(TAG,mobileip);
                        if (mobileip!=null) {
                            updatedevicelist(mobileip);
                            exibirProgress(true);
                        }
                        break;

                    case Constants.MSG_UPDATE_DEVICES_LIST:
                        String novodispositivo = resultData.getString(Constants.INTENT_KEY_REQUEST_RESULT);
                        deviceip=novodispositivo;
                        madapter.addAll(novodispositivo);
                    break;

                    case Constants.MSG_UPDATE_DEVICES_LIST_COMPLETE:
                        int progress = resultData.getInt(Constants.INTENT_KEY_REQUEST_PROGRESS);
                        if (progress == 100)
                        {
                            exibirProgress(false);
                        }
                    break;

                    case Constants.MSG_UPDATE_DEVICE_STATUS:
                    break;

                    case Constants.MSG_UPDATE_KWH:
                    break;

                }


            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getmobileip();
        devicestate=0;
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        listaDispositivos = (ListView) findViewById(R.id.lvdeviceslist);
        slistaDispositivos = new ArrayList<String>();
        madapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1, slistaDispositivos);
                listaDispositivos.setAdapter(madapter);
                listaDispositivos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String iptmp;
                        iptmp = listaDispositivos.getItemAtPosition(position).toString();
                        Log.d(TAG,iptmp);
                        Intent intent = new Intent(getApplicationContext(), DeviceActivity.class);
                        intent.putExtra(Constants.INTENT_KEY_DEVICE_IP, iptmp );
                        startActivity(intent);
                     }
                });
    }

    private void exibirProgress(boolean exibir) {
        mProgressBar.setVisibility(exibir ? View.VISIBLE : View.GONE);
    }


    private void getmobileip() {
        Log.d(TAG,"getmobileip");
        Intent intent = new Intent(this, WifiSearchService.class);
        intent.putExtra(Constants.INTENT_KEY_REQUEST_TYPE, Constants.MSG_MOBILE_IP );
        intent.putExtra(Constants.INTENT_KEY_RECEIVER, mReceiver);
        startService(intent);
    }

    private void updatedevicelist(String ip) {
        Intent intent = new Intent(this, WifiSearchService.class);
        intent.putExtra(Constants.INTENT_KEY_REQUEST_TYPE, Constants.MSG_UPDATE_DEVICES_LIST );
        intent.putExtra(Constants.INTENT_KEY_RECEIVER, mReceiver);
        intent.putExtra(Constants.INTENT_KEY_MOBILE_IP, ip);
        startService(intent);
    }

    private void findunregistereddevices() {

        Intent intent = new Intent(this, WifiSearchService.class);
        intent.putExtra(Constants.INTENT_KEY_REQUEST_TYPE, Constants.MSG_UPDATE_DEVICES_LIST );
        intent.putExtra(Constants.INTENT_KEY_RECEIVER, mReceiver);
        startService(intent);

    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

}
