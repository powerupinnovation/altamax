package treinamento.altamax;

import android.app.AlarmManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class DeviceActivity extends AppCompatActivity {

    private static final String TAG = WifiSearchService.class.getName();
    private ResultReceiver deviceReceiver;
    TextView DeviceName;
    TextView DeviceConsumoAtual;
    TextView DeviceConsumoAcumulado;
    ToggleButton Deviceswitch;
    Timer timer;
    TimerTask timerTask;
    String Deviceip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        DeviceName = (TextView) findViewById(R.id.txtDevname);
        DeviceConsumoAtual = (TextView) findViewById(R.id.txtDevConsumoAtual);
        DeviceConsumoAcumulado = (TextView) findViewById(R.id.txtDevconsumoacumulado);
        Deviceswitch = (ToggleButton) findViewById(R.id.btnDeviceswitch);

        Intent DeviceIntent = getIntent();
        final String Deviceip  = DeviceIntent.getStringExtra(Constants.INTENT_KEY_DEVICE_IP);

        DeviceName.setText(Deviceip);


        Deviceswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    switchDevice(Deviceip,"on");
                } else {
                    switchDevice(Deviceip,"off");
                }

            }
        });

        deviceReceiver = new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {

                int state = resultCode;
                switch (state) {

                    case Constants.MSG_UPDATE_DEVICE_STATUS:
                       /* String info = resultData.getString(Constants.INTENT_KEY_DEVICE_INFO);
                        Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();*/
                        break;

                    case Constants.MSG_UPDATE_KWH:
                        Log.d(TAG,"MSG_UPDATE_KWH");
                        String info = resultData.getString(Constants.INTENT_KEY_DEVICE_INFO);
                        String values [] = info.split(",");
                        String current [] = values[1].split(":");
                        DeviceConsumoAtual.setText(current[1]);
                        Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();
                        break;

                }
            }


        };

        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask(Deviceip);

        //schedule the timer, after the first 5000ms the TimerTask will run every 30000ms
        timer.schedule(timerTask, 0, 30000); //


        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);


    }

    @Override

    public void onStop(){
        stoptimertask();
        super.onStop();
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask(final String ip) {

        timerTask = new TimerTask() {
            public void run() {
                getStatus(ip);
            }
        };
    }

    private void switchDevice(String ip,String state) {
        Intent intent = new Intent(this, WifiSearchService.class);
        intent.putExtra(Constants.INTENT_KEY_REQUEST_TYPE, Constants.MSG_UPDATE_DEVICE_STATUS );
        intent.putExtra(Constants.INTENT_KEY_DEVICE_IP, ip );
        intent.putExtra(Constants.INTENT_KEY_DEVICE_STATUS, state );
        intent.putExtra(Constants.INTENT_KEY_RECEIVER, deviceReceiver);
        startService(intent);
    }

    private void getStatus(String ip) {
        Intent intent = new Intent(this, WifiSearchService.class);
        intent.putExtra(Constants.INTENT_KEY_REQUEST_TYPE, Constants.MSG_GET_DEVICE_STATUS );
        intent.putExtra(Constants.INTENT_KEY_DEVICE_IP, ip );
        intent.putExtra(Constants.INTENT_KEY_RECEIVER, deviceReceiver);
        startService(intent);
    }

}
