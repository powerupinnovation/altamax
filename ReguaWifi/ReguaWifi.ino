#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "EmonLib.h" 
#include <EEPROM.h>


#define LAST_STATUS_ADDRESS 2 
#define MAX_ENTRY_SIZE 20

const char* ssid = "MOTOROLA-0821D";
const char* password = "3f7f39850e22a0a319af";
ESP8266WebServer server(80);
EnergyMonitor emon1;                   // Create an instance

const int led = 13;

const char* Irmschar;
double Irms;
int status;

void handleRoot() {
  digitalWrite(led, 1);
  server.send(200, "text/plain", "hello from esp8266!");
  digitalWrite(led, 0);
}

void handleNotFound(){
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}

void setup(void){
  pinMode(2, OUTPUT);
  pinMode(A0, INPUT);
  emon1.current(A0, 2.26);             // Current: input pin, calibration.
  EEPROM.begin(512);
  status = EEPROM.read(LAST_STATUS_ADDRESS);
  if(status)
  {
    digitalWrite(2,HIGH);
  }else{
    digitalWrite(2,LOW);
   }
   digitalWrite(2,LOW);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/inline", [](){
    server.send(200, "text/plain", "this works as well");
  });

  server.on("/on", [](){
    status=1;
    digitalWrite(2,HIGH);
    EEPROM.write(LAST_STATUS_ADDRESS,status);
     Serial.println("on");
    server.send(200, "text/plain", "on");
  });

  server.on("/off", [](){
    status=0;
    digitalWrite(2,LOW);
    Serial.println("off");
    EEPROM.write(LAST_STATUS_ADDRESS,status);
    server.send(200, "text/plain", "off");
  });

  server.on("/getstatus", [](){
    
    String Output;
    //char* entry;
    if(status)
  {
    Output="Status:on,Corrente:";
    //server.send(200, "text/plain", "on");
  }else{
    Output="Status:off,Corrente:";
    //server.send(200, "text/plain", "off");
   }
   //const char* cString = Output.c_str();
  //strncpy(entry, cString, MAX_ENTRY_SIZE);
   String sIrms(Irms);
   Output+=sIrms; 
   server.send(200, "text/plain", Output);
    
  });
  
  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  server.handleClient();
  delay(1000);
  Irms = emon1.calcIrms(1996);  // Calculate Irms only
/*  
 *   /Serial.print(Irms*230.0);         // Apparent power
  Serial.print(" ");
 */
  Serial.println(Irms);
                  // Create an instance
}
